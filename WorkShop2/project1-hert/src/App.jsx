import { useState, useEffect } from "react";
import "./App.css";
import Header from "./components/Header";
import AddForm from "./components/AddForm";
import Item from "./components/Item";

function App() {
  const [tasks, setTasks] = useState(
    JSON.parse(localStorage.getItem("tasks")) || []
  );
  const [title, setTitle] = useState("");
  const [editId, setEditid] = useState(null);
  const [theme, setTheme] = useState("light");
  //รูปแบบ 3
  useEffect(() => {
    localStorage.setItem("tasks", JSON.stringify(tasks));
  }, [tasks]);

  function deleteTask(id) {
    const result = tasks.filter((item) => item.id !== id);
    setTasks(result);
    console.log(result);
  }

  function editTask(id) {
    setEditid(id);
    const editTask = tasks.find((item) => item.id === id);
    setTitle(editTask.title);
    console.log(editTask);
  }

  function saveTask(e) {
    e.preventDefault();
    if (!title) {
      alert("กรุณาป้อนข้อมูล");
    } else if (editId) {
      //อัพเดตข้อมูล
      const updateTask = tasks.map((item) => {
        if (item.id === editId) {
          return { ...item, title: title };
        }
        return item;
      });
      console.log(updateTask);
      setTasks(updateTask);
      setEditid(null);
    } else {
      //เพิ่มรายการใหม่
      const newTask = {
        id: Math.floor(Math.random() * 1000),
        title: title,
      };
      setTasks([...tasks, newTask]);
      setTitle("");
    }
  }
  return (
    <div className={"App " + theme}>
      <Header theme={theme} setTheme={setTheme}></Header>
      <div className="container">
        <AddForm
          title={title}
          setTitle={setTitle}
          saveTask={saveTask}
          editId={editId}
        ></AddForm>
        <section>
          {tasks.map((data) => (
            <Item
              key={data.id}
              data={data}
              deleteTask={deleteTask}
              editTask={editTask}
            ></Item>
          ))}
        </section>
      </div>
    </div>
  );
}

export default App;
