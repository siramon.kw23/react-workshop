import React, { useState, createContext } from "react";

import "./App.css";
import Header from "./components/header";
import StudentList from "./components/StudentList";
import AddFrom from "./components/AddFrom";

export const studentsContext = createContext();

function App() {
  const [students, setStudent] = useState([
    { id: 1, name: "A", gender: "male" },
    { id: 2, name: "B", gender: "female" },
    { id: 3, name: "C", gender: "female" },
  ]);

  return (
    <div className="container">
      <Header title="Home" />
      <main>
        <studentsContext.Provider value={{ students, setStudent }}>
          <AddFrom />
          <StudentList students={students} />
        </studentsContext.Provider>
      </main>
    </div>
  );
}

export default App;
