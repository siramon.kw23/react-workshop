import Logo from "../pic/book.png";
import "./Header.css";

export default function Header(props){
    const {title} = props
    return(
        <nav>
            <img src={Logo} alt="logo" />
            <br />
            <a href="/">{props.title}</a>
            
        </nav>
        
    );
}
