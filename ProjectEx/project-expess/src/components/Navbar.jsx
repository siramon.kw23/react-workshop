import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import SettingsIcon from "@mui/icons-material/Settings";
import "../CSS/Navbar.css";

export default function Navbar(props) {
  const { backIcon, detail } = props;

  return (
    <Box id="nav">
      <Button style={{ color: 'white' }}>{backIcon}</Button>

      {detail}
      <Button style={{ color: 'white' }}>
      <SettingsIcon />
      </Button>
      
    </Box>
  );
}
