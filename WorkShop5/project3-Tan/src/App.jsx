import Header from './conponent/Header'
import Cart from './conponent/Cart'
import { useState } from 'react';


function App() {
  const [count, setCount] = useState(0)

  return (
    <div className='App'>
      <Header/>
      <Cart/>
    </div>
  );
}

export default App
