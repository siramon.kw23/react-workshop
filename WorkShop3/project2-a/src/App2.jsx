import React from "react";
import "./App2.css";
import Home from "./components/Home";
import About from "./components/About";
import Blogs from "./components/Blogs";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import Navbar from "./components/Navbar";
import NotFound from "./components/NotFound";
import Detail from "./components/Detail";

const App2 = () => {
  return (
    <BrowserRouter>
      <Navbar />
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route path="/about" element={<About />}></Route>
        <Route path="/blogs" element={<Blogs />}></Route>
        <Route path="*" element={<NotFound />}></Route>
        <Route path="/home" element={<Navigate to="/" />}></Route>
        <Route path="/info" element={<Navigate to="/about" />}></Route>
        <Route path="/blog/:id" element={<Detail />}></Route>
      </Routes>
    </BrowserRouter>
  );
};

export default App2;
