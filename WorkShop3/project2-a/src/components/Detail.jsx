import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import data from "../data/data";
import "../components/Detail.css";

const Detail = () => {
  const { id } = useParams();
  const [title, setTitle] = useState("");
  const [image, setImage] = useState("");
  const [content, setContent] = useState("");
  const [author, setAuthor] = useState("");

  useEffect(() => {
    const detailData = data.find((data) => data.id === parseInt(id));
    console.log("data จาก ID ที่คลิกส่งมา ", detailData);
    setTitle(detailData.title);
    setImage(detailData.image_url);
    setContent(detailData.content);
    setAuthor(detailData.author);
  }, []);
  return (
    <div className="container">
      <h1 className="title">บทความ: {title} </h1>
      <img src={image} alt={title} />
      <div className="author">
        <h2>Author: {author}</h2>
      </div>
      <hr />
      <p>เนื้อหา: {content}</p>
    </div>
  );
};

export default Detail;
