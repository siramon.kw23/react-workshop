import notFound from "../images/404.png";
export default function NotFound() {
  return (
    <div className="container">
      <h3 className="title">ไม่พบหน้าเว็บ (404 Not Found)</h3>
      <img src={notFound} alt="Not found page" />
    </div>
  );
}
