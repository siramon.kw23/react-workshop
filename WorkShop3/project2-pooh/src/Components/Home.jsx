import eiei from "../Images/eiei.jpg";

export default function Home() {
  return (
    <div className="container">
      <h2>First page of Website!</h2>
      <img src={eiei} alt="home" />
    </div>
  );
}
