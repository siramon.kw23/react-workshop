import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import { Avatar, IconButton, Typography, AvatarGroup } from "@mui/material";
import NotificationsNoneIcon from "@mui/icons-material/NotificationsNone";
import AddIcon from "@mui/icons-material/Add";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Fab from "@mui/material/Fab";
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
export default function Task() {
  const [value, setValue] = React.useState(0);
  return (
    <React.Fragment>
      <CssBaseline />
      <Container
        maxWidth="lg"
        sx={{ p: 2, bgcolor: "#131C2F", height: "100vh" }}
      >
        <Box display="flex" justifyContent="space-between" sx={{ m: 2 }}>
          <Avatar
            alt="Eunchae"
            src="https://www.allkpop.com/upload/2023/02/content/030058/1675403924-20230203-hongeunchae.jpg"
          />
          <Box display="flex">
            <Box sx={{ bgcolor: "#203359", borderRadius: "50%" }}>
              <IconButton>
                <NotificationsNoneIcon sx={{ color: "white" }} />
              </IconButton>
            </Box>
            <Box sx={{ bgcolor: "#203359", borderRadius: "50%", ml: 0.5 }}>
              <IconButton>
                <AddIcon sx={{ color: "white" }} />
              </IconButton>
            </Box>
          </Box>
        </Box>
        <Box
          sx={{
            width: "100%",
            display: "flex",
            justifyContent: "space-around",
            mb: 3,
          }}
        >
          <Tabs
            value={value}
            onChange={(e, val) => setValue(val)}
            centered
            TabIndicatorProps={{
              sx: {
                backgroundColor: "white",
                gridTemplateColumns: "repeat(2, 1fr)",
                gap: 1,
                display: "grid",
              },
            }}
          >
            <Tab label="Tasks" sx={{ color: "white" }}></Tab>
            <Tab label="Boards" sx={{ color: "white" }}></Tab>
          </Tabs>
        </Box>
        <Box bgcolor="#A9ECFF" sx={{ p: 2 }} borderRadius="20px">
          <Box display="flex" justifyContent="space-between">
            <Box display="flex">
              <Box sx={{ bgcolor: "#85C1D3", borderRadius: "50%", ml: 0.5 }}>
                {" "}
                <IconButton>
                  <AddIcon />
                </IconButton>
              </Box>
              <Avatar
                alt="Eunchae"
                src="https://www.allkpop.com/upload/2023/02/content/030058/1675403924-20230203-hongeunchae.jpg"
                sx={{ ml: 2 }}
              />
            </Box>
            <Box>
              <IconButton>
                <MoreHorizIcon />
              </IconButton>
            </Box>
          </Box>
          <Box>
            <Typography sx={{ fontSize: "13px", color: "#787878", my: 1 }}>
              2 Active Tasks
            </Typography>
            <Typography variant="h4" sx={{ fontWeight: "500px" }}>
              Myself
            </Typography>
          </Box>
        </Box>
        <Box bgcolor="#FDF96C" sx={{ p: 2 }} borderRadius="20px">
          <Box display="flex" justifyContent="space-between">
            <Box display="flex">
              <Box sx={{ bgcolor: "#D9D55F", borderRadius: "50%", ml: 0.5 }}>
                {" "}
                <IconButton>
                  <AddIcon />
                </IconButton>
              </Box>
              <AvatarGroup max={2} sx={{ ml: 1 }}>
                <Avatar
                  alt="Eunchae"
                  src="https://www.allkpop.com/upload/2023/02/content/030058/1675403924-20230203-hongeunchae.jpg"
                />
                <Avatar
                  alt="Chaewon"
                  src="https://www.nme.com/wp-content/uploads/2022/04/le-sserafim-kim-chae-won-source-music-girl-group-2022-696x442.jpg"
                />
              </AvatarGroup>
            </Box>
            <Box>
              <IconButton>
                <MoreHorizIcon />
              </IconButton>
            </Box>
          </Box>
          <Box>
            <Typography sx={{ fontSize: "13px", color: "#787878", my: 1 }}>
              2 Active Tasks
            </Typography>
            <Typography variant="h4" sx={{ fontWeight: "500px" }}>
              Myself
            </Typography>
          </Box>
        </Box>
        <Box position="fixed" bottom="0" left="50%" sx={{ transform:" translate(-50%,-50%)"}}>
          <Fab color="primary" aria-label="add">
            <AddIcon />
          </Fab>
        </Box>
      </Container>
    </React.Fragment>
  );
}
