import { Box } from "@mui/material";
import Grid from "@mui/material/Grid";
import "./App.css";
import Typography from "@mui/material/Typography";
import KeyboardBackspaceIcon from "@mui/icons-material/KeyboardBackspace";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import AltRouteIcon from "@mui/icons-material/AltRoute";
import SignalCellularAltIcon from "@mui/icons-material/SignalCellularAlt";
import AccessTimeIcon from "@mui/icons-material/AccessTime";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";

function App() {
  return (
    <>
      <Grid container spacing={2} direction="column">
        <Grid item sm={12} className="photo" direction="column">
          <Box
            item
            sm={12}
            display="flex"
            direction="row"
            justifyContent="space-between"
            align-items="center"
            width="100%"
          >
            <KeyboardBackspaceIcon />
            <Typography fontWeight="bold">Msersedes Benz</Typography>
            <MoreHorizIcon />
          </Box>
          <Box>
            <Typography
              margin={1}
              border="1px solid #00000040"
              padding={0.4}
              borderRadius={2}
              fontSize={12}
              fontWeight="bold"
              backgound-color="#fff"
            >
              E-Class
            </Typography>
          </Box>
          <img src="car.png" className="png"></img>
          <Box className="content">
            <div className="logo">
              <img src="logo.jpg"></img>
            </div>
            <Grid
              container
              xs={12}
              textAlign="center"
              marginLeft="auto"
              marginRight="auto"
              padding="0px 15px"
              position="relative"
              top={-10}
            >
              <Grid item xs={4}>
                <AltRouteIcon />
                <Typography
                  margin={1}
                  fontSize={16}
                  fontWeight="bold"
                  position="relative"
                  top={-10}
                >
                  328 ml
                </Typography>
                <Typography
                  margin={1}
                  fontSize={14}
                  position="relative"
                  top={-20}
                  color="grey"
                >
                  Range
                </Typography>
              </Grid>
              <Grid item xs={4}>
                <SignalCellularAltIcon />
                <Typography
                  margin={1}
                  fontSize={16}
                  fontWeight="bold"
                  position="relative"
                  top={-10}
                >
                  155 mph
                </Typography>
                <Typography
                  margin={1}
                  fontSize={14}
                  position="relative"
                  top={-20}
                  color="grey"
                >
                  Top Speed
                </Typography>
              </Grid>
              <Grid item xs={4} paddingLeft="auto">
                <AccessTimeIcon />
                <Typography
                  margin={1}
                  fontSize={16}
                  fontWeight="bold"
                  position="relative"
                  top={-10}
                >
                  4.4s
                </Typography>
                <Typography
                  margin={0}
                  fontSize={14}
                  position="relative"
                  top={-20}
                  color="grey"
                >
                  0-60mph
                </Typography>
              </Grid>
              <hr />
              <Grid container xs={12} marginLeft="auto" marginRight="auto">
                <Grid item xs={6}>
                  <Typography
                    margin={1}
                    fontSize={15}
                    fontWeight="bold"
                    position="relative"
                    textAlign="left"
                  >
                    Transmissions
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography
                    margin={1}
                    fontSize={15}
                    fontWeight="bold"
                    position="relative"
                    textAlign="right"
                  >
                    Automatic
                    <ArrowForwardIosIcon sx="font-size:15px; top:2px; position:relative;" />
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          </Box>
          <Box className="BoxCard">
            <Grid
              container
              xs={12}
              textAlign="center"
              position="relative"
              top={-12}
              left={4}
              spacing={1}
            >
              <Grid item xs={6}>
                <Box className="card">
                  <Box className="headcard">
                    <Typography fontSize={15} fontWeight="bold" margin={1.5}>
                      Activity
                    </Typography>
                    <Typography>
                      <ArrowForwardIosIcon sx="font-size:15px; top:8px; position:relative; margin:8px;" />
                    </Typography>
                  </Box>
                  <div className="incard1">~ 8%</div>
                  <Typography
                    margin={1}
                    fontSize={20}
                    fontWeight="bold"
                    position="relative"
                  >
                    145 mph
                  </Typography>
                  <Typography
                    fontSize={12}
                    position="relative"
                    top={-10}
                    color="grey"
                  >
                    Traveled
                  </Typography>
                </Box>
              </Grid>
              <Grid item xs={6}>
                <Box className="card">
                  <Box className="headcard">
                    <Typography fontSize={15} fontWeight="bold" margin={1.5}>
                      Gas volume
                    </Typography>
                    <Typography>
                      <ArrowForwardIosIcon sx="font-size:15px; top:8px; position:relative; margin:8px;" />
                    </Typography>
                  </Box>
                  <div className="incard2"></div>
                  <Typography
                    margin={1}
                    fontSize={20}
                    fontWeight="bold"
                    position="relative"
                  >
                    63%
                  </Typography>
                  <Typography
                    margin={0}
                    fontSize={12}
                    position="relative"
                    top={-10}
                    color="grey"
                  >
                    I can drive 178 miles
                  </Typography>
                </Box>
              </Grid>
              <Grid item xs={6}>
                <Box className="card">
                  <Box className="headcard">
                    <Typography fontSize={15} fontWeight="bold" margin={1.5}>
                      Details
                    </Typography>
                    <Typography>
                      <ArrowForwardIosIcon sx="font-size:15px; top:8px; position:relative; margin:8px;" />
                    </Typography>
                  </Box>
                </Box>
              </Grid>
              <Grid item xs={6} display="flex" justifyContent="right">
                <Box className="card">
                  <Box className="headcard">
                    <Typography fontSize={15} fontWeight="bold" margin={1.5}>
                      Customize
                    </Typography>
                    <Typography>
                      <ArrowForwardIosIcon sx="font-size:15px; top:8px; position:relative; margin:8px;" />
                    </Typography>
                  </Box>
                </Box>
              </Grid>
              <Grid item xs={12}>
                <Box className="booking">
                  <Typography
                    fontSize={15}
                    fontWeight="bold"
                    position="relative"
                    padding={1.5}
                  >
                    Booking car
                  </Typography>
                  <Typography fontSize={13} position="relative" padding={1.5}>
                    $750 /month
                  </Typography>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Grid>
      </Grid>
    </>
  );
}

export default App;
