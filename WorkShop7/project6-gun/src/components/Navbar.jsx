import * as React from "react";
import BottomNavigation from "@mui/material/BottomNavigation";
import BottomNavigationAction from "@mui/material/BottomNavigationAction";
import FolderIcon from "@mui/icons-material/Folder";
import RestoreIcon from "@mui/icons-material/Restore";
import FavoriteIcon from "@mui/icons-material/Favorite";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import { Box } from "@mui/material";
export default function Navbar() {
  const [value, setValue] = React.useState("recents");

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box sx={{borderRadius:"0 0 500px 500px " ,overflow:"hidden"}}>
      <BottomNavigation
        sx={{
          width: "98vw",
          backgroundColor: "#7524D2",
          borderRadius: "10px 10px",
        }}
        value={value}
        onChange={handleChange}
      >
        <BottomNavigationAction
          value="recents"
          icon={<RestoreIcon />}
          sx={{ backgroundColor: "#7524D2", color: "#fff" }}
        />
        <BottomNavigationAction
          value="favorites"
          icon={<FavoriteIcon />}
          sx={{ backgroundColor: "#7524D2", color: "#fff" }}
        />
        <BottomNavigationAction
          value="nearby"
          icon={<LocationOnIcon />}
          sx={{ backgroundColor: "#7524D2", color: "#fff" }}
        />
        <BottomNavigationAction
          value="folder"
          icon={<FolderIcon />}
          sx={{ backgroundColor: "#7524D2", color: "#fff" }}
        />
      </BottomNavigation>
    </Box>
  );
}
