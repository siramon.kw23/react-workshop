import { useState } from 'react'
import Header from "./Components/Header";
import './App.css'
import Cart from './Components/Cart';


function App() {
  const [count, setCount] = useState(0)

  return (
    <div className='App'>
      <Header/>
      <Cart/>
    </div>
  )
}

export default App
