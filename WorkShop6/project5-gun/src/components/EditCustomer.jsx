import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import { Typography, Grid } from "@mui/material";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";



export default function CreateCustomer() {
  const { id } = useParams();
  const [customer_name, setCustomer_name] = useState("");
  const [phone_number, setPhone_number] = useState("");

  useEffect(() => {
    var requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    fetch(
      "https://4ef6-202-28-119-38.ngrok-free.app/customer/" + id,
      requestOptions
    )
      .then((response) => response.json())
      .then((result) => {
        setCustomer_name(result["customer_name"]);
        setPhone_number(result["phone_number"]);
      })
      .catch((error) => console.log("error", error));
  }, [id]);

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log(id,customer_name,phone_number)
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    
    var raw = JSON.stringify({
      "customer_name": customer_name,
      "phone_number": phone_number
    });
    
    var requestOptions = {
      method: 'PUT',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    
    fetch("https://4ef6-202-28-119-38.ngrok-free.app/customer/"+id, requestOptions)
      .then(response => response.text())
      .then(()=>{
        alert('updated')
        window.location.href='/'
      })
      .catch(error => console.log('error', error));
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="sm" sx={{ p: 2 }}>
        <Typography variant="h6" gutterBottom>
          Create User
        </Typography>
        <form onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                id="customer_name"
                label="Name"
                variant="outlined"
                fullWidth
                required
                onChange={(e) => setCustomer_name(e.target.value)}
                value={customer_name}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="phone_number"
                label="Phone number"
                variant="outlined"
                fullWidth
                required
                onChange={(e) => setPhone_number(e.target.value)}
                value={phone_number}
              />
            </Grid>

            <Grid item xs={12}>
              <Button type="submit" variant="contained" fullWidth >
                Create
              </Button>
            </Grid>
          </Grid>
        </form>
      </Container>
    </React.Fragment>
  );
}
