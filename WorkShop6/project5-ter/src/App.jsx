import "./App.css";
import Navbar from "./components/Navbar";
import Home from "./components/Home";
import { Route, Routes, BrowserRouter } from "react-router-dom";
import Cards from "./components/Cards";
import Tables from "./components/Tables";
import Usercreate from "./components/Usercreate";
import Userupdate from "./components/Userupdate";
function App() {
  return (
    <>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="card" element={<Cards />} />
          <Route path="table" element={<Tables />} />
          <Route path="create" element={<Usercreate />} />
          <Route path="update/:id" element={<Userupdate />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
