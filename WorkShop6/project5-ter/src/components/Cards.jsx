import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import {
  Box,
  Typography,
  Button,
  Link,
  Card,
  CardActions,
  CardContent,
  ButtonGroup,
  Grid,
  Avatar
} from "@mui/material";
import { useState, useEffect } from "react";
import Container from "@mui/material/Container";

export default function Cards() {
  const [items, setItems] = useState([]);

  useEffect(() => {
    userGet();
  }, []);

  const userGet = () => {
    fetch("https://www.melivecode.com/api/users")
      .then((res) => res.json())
      .then((result) => {
        setItems(result);
      });
  };

  const Userupdate = (id) => {
    window.location = "/update/" + id;
  };
  const Userdelete = (id) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      id: id,
    });

    var requestOptions = {
      method: "DELETE",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch("https://www.melivecode.com/api/users/delete", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        alert(result["message"]);
        if (result["status"] === "ok") {
          userGet();
        }
      })
      .catch((error) => console.log("error", error));
  };
  return (
    <React.Fragment>
      <CssBaseline />
      <Container>
        <Box display="flex" sx={{ m: 2 }}>
          <Box sx={{ flexGrow: 1 }}>
            <Typography variant="h6" gutterBottom>
              Card
            </Typography>
          </Box>
          <Link href="create">
            <Button variant="contained">Create</Button>
          </Link>
        </Box>
        <Grid
          container
          spacing={2}
          direction="row"
          justifyContent="center"
          alignItems="center"
        >
          {items.map((row) => (
            <Grid item xs={12} md={4} key={row.id}>
              <Card>
                <CardContent>
                  <Box>
                    <Avatar alt={row.username} src={row.avatar} />
                  </Box>
                  <Typography variant="h5" component="div">
                    {row.fname}
                  </Typography>
                  <Typography sx={{ mb: 1.5 }} color="text.secondary">
                    {row.lname}
                  </Typography>
                  <Typography variant="body2">{row.username}</Typography>
                  <CardActions>
                    <ButtonGroup
                      variant="outlined"
                      aria-label="outlined button group"
                    >
                      <Button onClick={() => Userupdate(row.id)}>Edit</Button>
                      <Button onClick={() => Userdelete(row.id)}>Del</Button>
                    </ButtonGroup>
                  </CardActions>
                </CardContent>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Container>
    </React.Fragment>
  );
}
