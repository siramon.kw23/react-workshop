import logo from "./logo.svg";
import "./Calculator.css";
import { useState } from "react";

export default function Calculator() {
  const [numerator, setNumerator] = useState("");
  const [denominator, setDenominator] = useState("");
  const [oparator, setOparator] = useState("add");
  const [result, setResult] = useState();

  function calDigit(e) {
    e.preventDefault();
    if (!numerator || !denominator) {
      alert("Please enter your numbers to calculator");
    } else if (oparator === "add") {
      setResult(parseFloat(numerator) + parseFloat(denominator));
    } else if (oparator === "substract") {
      setResult(parseFloat(numerator) - parseFloat(denominator));
    } else if (oparator === "multiple") {
      setResult(parseFloat(numerator) * parseFloat(denominator));
    } else if (oparator === "divide") {
      setResult(parseFloat(numerator) / parseFloat(denominator));
    }
  }
  function resetCal() {
    setNumerator("");
    setDenominator("");
    setOparator("add");
    setResult();
  }

  return (
    <div>
      <div className="header">
        <img src={logo} alt="Calculator Logo" />
        <h1>Calculator</h1>
      </div>

      <div className="container">
        <h2>*Insert numbers that you want to calculator</h2>
        <form onSubmit={calDigit}>
          <input
            type="float"
            name="numerator"
            value={numerator}
            onChange={(e) => setNumerator(e.target.value)}
            className="inputelement"
          />
          <select
            value={oparator}
            onChange={(e) => setOparator(e.target.value)}
            className="operator inputelement"
          >
            <option value="add">+</option>
            <option value="substract">-</option>
            <option value="multiple">x</option>
            <option value="divide">÷</option>
          </select>
          <input
            type="float"
            name="denominator"
            value={denominator}
            onChange={(e) => setDenominator(e.target.value)}
            className="inputelement"
          />
          <div className="btnClick">
            <button type="submit" className="btn btn-cal">
              Calculate
            </button>
            <button type="button" onClick={resetCal} className="btn btn-reset">
              Clear
            </button>
          </div>
        </form>

        <div className="result">
          Result : <span>{result}</span>
        </div>
      </div>
    </div>
  );
}
