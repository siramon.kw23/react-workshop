import me from "./me.jpg";
import './Workshop1.css';

function Workshop1() {
    return (
      <body style={{ display: "block"}}>
        <h1 className="head">Portfolio</h1>
        <br />
        <img
            src={me}
            style={{ display: "block", borderRadius: "50px"}}
            className="App-logo"
            alt="cat"
        />
        <h1 style={{textAlign:"center"}}>Juthaporn Thanoi</h1>
        
        <div className="content">
          <h2> Nickname : Baew</h2>
          <h2> Position : Front-end</h2>
          <h2>
            Skill : <br />
            <ul>
              <li>html</li>
              <li>css</li>
              <li>Javascript</li>
            </ul>
          </h2>
          <h2>
            Hobby : <br />
            <ul>
              <li>movie</li>
              <li>music</li>
            </ul>
          </h2>
        </div>
       
        <br />
      </body>
    );
  }
  
  export default Workshop1;