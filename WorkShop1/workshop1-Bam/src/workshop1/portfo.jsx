import "../port.css";
import ProfileImage from "../image/bamm.jpg";

function Portfolio() {
  return (
    <div className="Portfolio">
      <div className="head">
        <h1>Portfolio</h1>
        <img src={ProfileImage} alt="Profile Image" />
        <h2 className="name">Siramol Skolchaiphraison</h2>
      </div>
      <div className="body">
        
        <h3>Nickname : Bam</h3>
        <h3>Position : Front End Developer</h3>
        <h3 className="skill">Skill
            <ul className="skill-li">
                <li>HTML</li>
                <li>CSS</li>
                <li>Javascript</li>
            </ul>
        </h3>
        
        <h3 className="hobby">My hobby
        <ul className="hobby-li">
            <li>Play Guitar</li>
            <li>Sleep</li>
            <li>Play Game</li>
        </ul>
        </h3>
        
      </div>
    </div>
  );
}

export default Portfolio
