import { useState } from "react";
import Item from "./Item";
export default function Stu_list() {
  const [students, setStudent] = useState([{ id: 1, name: "arm" }]);
  const [show, setShow] = useState(true);
  function delStu(id) {
    setStudent(students.filter((item) => item.id !== id));
  }
  return (
    <>
      <h1>จำนวนนักเรียน = {students.length}</h1>
      <button onClick={() => setShow(!show)}>{show ? "ซ่อน":"แสดง"}</button>
      <ul>
        {show &&
          students.map((data) => (
            <Item key={data.id} data={data} delStu={delStu}/>
          ))}
      </ul>
    </>
  );
}
