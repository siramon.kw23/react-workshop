import Profileimg from "../img/profile.jpg"
import "./Ws01.css"
function Ws01(){
    return (
        <>
          <div className="header">
            <img className="image" src={Profileimg} alt="" />
            <h1> นายธีรพงศ์ สังกะ </h1>
          </div>
          <div className="detail">
            <p><span>ชื่อเล่น:</span> เตอร์</p>
            <p><span>ตำแหน่ง:</span> Front-end</p>
            <p><span>skill:</span></p>
            <ul>
              <li>html</li>
              <li>css</li>
              </ul>
            <p><span>งานอดิเรก:</span></p>
            <ul>
              <li>นอน</li>
              <li>เล่นเกม</li>
              <li>อ่านการ์ตูน</li>
            </ul>
          </div>
        </>
      )
    }
    
    export default Ws01
