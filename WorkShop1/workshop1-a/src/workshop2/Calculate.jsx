import React , {useState, useEffect}from 'react'
import '../css/Calculate.css'

const Calculate = () => {
    const [value, setValue] = useState(0);
    const [data, setData] = useState(
      {
        data1: 0,
        data2: 0,
        symbol: '+'
      }
    )
    const handleOnchange = (event) => {
      const { name, value } = event.target;
      console.log(value)
      setData(prevData => ({
        ...prevData,
        [name]: value
      }))
    }

    useEffect(()=>{
      let result;

      if(data.symbol === "+") {
        result = parseFloat(data.data1) + parseFloat(data.data2);
      } else if(data.symbol === "-") {
        result = parseFloat(data.data1) - parseFloat(data.data2);
      } else if(data.symbol === "*") {
        result = parseFloat(data.data1) * parseFloat(data.data2);
      } else if(data.symbol === "/") {
        result = parseFloat(data.data1) / parseFloat(data.data2);
      } else {
        result = 0;
      }
      setValue(result)
      console.log(result)
    },[data])
    // const showResult = () => {
    //   let result;
    //   if(data.symbol === "+") {
    //     result = parseFloat(data.data1) + parseFloat(data.data2);
    //   } else if(data.symbol === "-") {
    //     result = parseFloat(data.data1) - parseFloat(data.data2);
    //   } else if(data.symbol === "*") {
    //     result = parseFloat(data.data1) * parseFloat(data.data2);
    //   } else if(data.symbol === "/") {
    //     result = parseFloat(data.data1) / parseFloat(data.data2);
    //   } else {
    //     result = 0;
    //   }
    //   setValue(result)
    //   console.log(result)
    // }
  
  
  return (
    <div className='container-calculate'>
      <div className='name'>Calculator</div>
      <div className='box-calculate'>
  
        <div className='box-input'>
          <label className='name-content'>AddValue 1</label>
          <input type="number" name='data1' value={data.data1} onChange={handleOnchange} required/>
        </div>

        <div className='box-operator'>
            <select name="symbol" id="operator" value={data.symbol} onChange={handleOnchange}>
              <option value="+">+</option>
              <option value="-">-</option>
              <option value="*">*</option>
              <option value="/">/</option>
            </select>
        </div>
        
        <div className='box-input'>
          <label className='name-content'>AddValue 2</label>
          <input type="number"  name='data2' value={data.data2} onChange={handleOnchange} required/>
        </div>
      </div>
      <div className='box-button-clear'>
          <button onClick={() => {setData({
            data1:0,
            data2:0,
            symbol:'+'
          }) & setValue(0)}}>Clear</button>
      </div>
      <div className='box-input-result' style={{ marginTop: '20px', display: 'flex', flexDirection: 'row' }}>
          <div className='result-content' style={{margin: '0 10px'}}>Result</div>
          <label className='value-result'>{value?value:0}</label>
      </div>
      
    </div>
  )
}

export default Calculate

