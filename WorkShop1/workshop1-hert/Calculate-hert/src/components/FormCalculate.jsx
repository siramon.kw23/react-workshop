import { useState } from "react";
import "../CSS/FormCalculate.css";

function FromCalculate() {
  const [value1, setValue1] = useState(0);
  const [value2, setValue2] = useState(0);
  const [operator, setOperator] = useState("+");
  const [result, setResult] = useState(0);

  const Calculate = () => {
    if (operator === "+") {
      console.log(typeof(value1));
      console.log(typeof(value2));
      setResult(parseFloat(value1) + parseFloat(value2));
    } else if (operator === "-") {
      setResult(parseFloat(value1) - parseFloat(value2));
    } else if (operator === "*") {
      setResult(parseFloat(value1) * parseFloat(value2));
    } else if (operator === "/") {
      setResult(parseFloat(value1) / parseFloat(value2));
    }
    console.log(result);
  };

  console.log(value1);
  console.log(value2);
  console.log(operator);


  return (
    <div>
      <div className="ContentContainer col-12">
        <div className="ContentTextStyle">Calculate</div>
      </div>

      <div className="FormContainer FormBorder col-12">
        <div>
          <label className="InputTextStyle">Input Value</label>
          <input
            type="number"
            name="value1"
            value={value1}
            onChange={(e) => setValue1(e.target.value)}
          ></input>
        </div>

        <div className="OperatorContainer">
          <select
            value={operator}
            onChange={(e) => setOperator(e.target.value)}
          >
            <option value="+">+</option>
            <option value="-">-</option>
            <option value="*">*</option>
            <option value="/">/</option>
          </select>
        </div>

        <div>
          <label className="InputTextStyle">Input Value</label>
          <input
            type="number"
            name="value2"
            value={value2}
            onChange={(e) => setValue2(e.target.value)}
          ></input>
        </div>

        <button
          type="submit"
          className="OperatorContainer"
          onClick={Calculate}
        >
          Calculate
        </button>
      </div>

      <div className="ContentContainer col-12">
        <div className="ContentTextStyle">Result : {result}</div>
      </div>
    </div>
  );
}

export default FromCalculate;
